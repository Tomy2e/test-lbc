package car

import (
	"errors"
	"strings"

	"github.com/texttheater/golang-levenshtein/levenshtein"
	"gitlab.com/Tomy2e/test-lbc/pkg/types"
	godiacritics "gopkg.in/Regis24GmbH/go-diacritics.v2"
)

// normalize normalizes an input string by: removing all white spaces,
// replacing all diacritical characters with their ASCII equivalent, and
// making it lowercase.
func normalize(input string) string {
	// Remove all spaces
	input = strings.ReplaceAll(input, " ", "")

	// Remove diacritical characters and replace them with their ASCII representation.
	input = godiacritics.Normalize(input)

	// Return lowercase normalized input.
	return strings.ToLower(input)
}

// ErrNoModelFound is returned when no car model is found.
var ErrNoModelFound = errors.New("no car model found from the specified model")

// MatchWithModel takes an input model and returns the associated CarModel.
// The input model doesn't have to match exactly the internal car models.
// An error is returned if there is no match.
func MatchWithModel(inputModel string) (types.CarModel, error) {
	// Normalize the input model.
	nModel := normalize(inputModel)

	// This map holds candidate models.
	candidates := make(map[string]map[string]int)

	// Filter out models that do not match the input model.
	for brand, models := range BrandsAndModels {
		for _, m := range models {
			nm := normalize(m)
			if strings.Contains(nModel, nm) {
				if _, ok := candidates[brand]; !ok {
					candidates[brand] = make(map[string]int)
				}

				prefix := 0

				// If the input model has the model as its prefix, we assume
				// the input model fully corresponds to the model and we take
				// the candidate with longest prefix length.
				if strings.HasPrefix(nModel, nm) {
					prefix = len(nm)
				}

				candidates[brand][m] = prefix
			}
		}
	}

	// No candidate models found, return now.
	if len(candidates) == 0 {
		return types.CarModel{}, ErrNoModelFound
	}

	minDist := 0
	started := false
	var bestModel string
	var bestBrand string

	maxPrefix := 0
	var bestModelPref string
	var bestBrandPref string

	// Now we have one or more candidates, we use levenshtein distance to
	// select the best candidate.
	for brand, models := range candidates {
		for model, prefix := range models {
			if prefix > maxPrefix {
				bestBrandPref = brand
				bestModelPref = model

				maxPrefix = prefix
			}

			dist := levenshtein.DistanceForStrings([]rune(model), []rune(inputModel), levenshtein.DefaultOptions)

			if !started || dist < minDist {
				bestBrand = brand
				bestModel = model

				minDist = dist
				started = true
			}
		}
	}

	if maxPrefix > 0 {
		return types.CarModel{
			Brand: types.CarBrand{
				Brand: bestBrandPref,
			},
			Model: bestModelPref,
		}, nil
	}

	return types.CarModel{
		Brand: types.CarBrand{
			Brand: bestBrand,
		},
		Model: bestModel,
	}, nil
}
