package car

import (
	"reflect"
	"testing"

	"gitlab.com/Tomy2e/test-lbc/pkg/types"
)

func TestMatchWithModel(t *testing.T) {
	t.Parallel()
	type args struct {
		inputModel string
	}
	tests := []struct {
		name    string
		args    args
		want    types.CarModel
		wantErr bool
	}{
		{
			name: "rs4 avant",
			args: args{
				inputModel: "rs4 avant",
			},
			want: types.CarModel{
				Brand: types.CarBrand{
					Brand: "Audi",
				},
				Model: "Rs4",
			},
		},
		{
			name: "Gran Turismo Série5",
			args: args{
				inputModel: "Gran Turismo Série5",
			},
			want: types.CarModel{
				Brand: types.CarBrand{
					Brand: "BMW",
				},
				Model: "Serie 5",
			},
		},
		{
			name: "ds 3 crossback",
			args: args{
				inputModel: "ds 3 crossback",
			},
			want: types.CarModel{
				Brand: types.CarBrand{
					Brand: "Citroen",
				},
				Model: "Ds3",
			},
		},
		{
			name: "CrossBack ds 3",
			args: args{
				inputModel: "CrossBack ds 3",
			},
			want: types.CarModel{
				Brand: types.CarBrand{
					Brand: "Citroen",
				},
				Model: "Ds3",
			},
		},
		{
			name: "206",
			args: args{
				inputModel: "206",
			},
			want:    types.CarModel{},
			wantErr: true,
		},
		{
			name: "C15D",
			args: args{
				inputModel: "C15D",
			},
			want: types.CarModel{
				Brand: types.CarBrand{
					Brand: "Citroen",
				},
				Model: "C15",
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			got, err := MatchWithModel(tt.args.inputModel)
			if (err != nil) != tt.wantErr {
				t.Errorf("MatchWithModel() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MatchWithModel() = %v, want %v", got, tt.want)
			}
		})
	}
}
