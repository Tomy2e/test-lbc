package types

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	gonanoid "github.com/matoous/go-nanoid/v2"
)

// Advert is the core data of an advert.
type Advert struct {
	ID      string `gorm:"primaryKey" json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`

	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

// NewID generates a new ID for the advert.
func (a *Advert) NewID() (err error) {
	a.ID, err = gonanoid.New()

	return
}

// Validate validates the advert fields.
func (a *Advert) Validate() error {
	return validation.ValidateStruct(a,
		validation.Field(&a.Title, validation.Required, validation.Length(1, 255)),
		validation.Field(&a.Content, validation.Required, validation.Length(1, 1024)),
	)
}

// PropertyAdvert is an advert for a property.
type PropertyAdvert struct {
	AdvertID string `gorm:"primaryKey" json:"-"`
	Advert   Advert `json:"advert"`
}

// JobAdvert is an advert for a job.
type JobAdvert struct {
	AdvertID string `gorm:"primaryKey" json:"-"`
	Advert   Advert `json:"advert"`
}

// CarAdvert is an advert for a car.
type CarAdvert struct {
	AdvertID string `gorm:"primaryKey" json:"-"`
	Advert   Advert `json:"advert"`

	CarModel string   `gorm:"size:191" json:"carModel"`
	CarBrand string   `gorm:"size:191" json:"carBrand"`
	Car      CarModel `gorm:"foreignKey:CarBrand,CarModel;references:CarBrand,Model" json:"-"`
}
