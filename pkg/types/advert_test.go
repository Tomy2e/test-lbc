package types

import (
	"testing"
	"time"
)

func TestAdvert_Validate(t *testing.T) {
	t.Parallel()
	type fields struct {
		ID        string
		Title     string
		Content   string
		CreatedAt time.Time
		UpdatedAt time.Time
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "completely empty",
			fields:  fields{},
			wantErr: true,
		},
		{
			name: "missing title",
			fields: fields{
				Content: "test",
			},
			wantErr: true,
		},
		{
			name: "missing content",
			fields: fields{
				Title: "test",
			},
			wantErr: true,
		},
		{
			name: "nothing is missing",
			fields: fields{
				Title:   "ok",
				Content: "test",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			a := &Advert{
				ID:        tt.fields.ID,
				Title:     tt.fields.Title,
				Content:   tt.fields.Content,
				CreatedAt: tt.fields.CreatedAt,
				UpdatedAt: tt.fields.UpdatedAt,
			}
			if err := a.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("Advert.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
