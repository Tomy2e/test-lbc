package types

// CarBrand contains information for a car brand (e.g. Audi, BMW, etc.).
type CarBrand struct {
	Brand string `gorm:"primaryKey"`

	Models []CarModel `gorm:"foreignKey:CarBrand"`
}

// CarModel contains information for a car model. The car model is necessarily
// linked to a car brand.
type CarModel struct {
	CarBrand string   `gorm:"primaryKey"`
	Brand    CarBrand `gorm:"foreignKey:CarBrand;references:Brand"`

	Model string `gorm:"primaryKey"`
}
