package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/Tomy2e/test-lbc/pkg/car"
	"gitlab.com/Tomy2e/test-lbc/pkg/dao"
	"gitlab.com/Tomy2e/test-lbc/pkg/types"
)

// GetJobAdverts returns a gin handler func to list all job adverts.
func (h *Handler) GetJobAdverts() gin.HandlerFunc {
	return func(c *gin.Context) {
		ja, err := h.dao.GetJobAdverts()
		if err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse(ja))
		return
	}
}

// GetJobAdvert returs a gin handler func to get a job advert by ID.
func (h *Handler) GetJobAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		ja, err := h.dao.GetJobAdvertByID(c.Param("id"))
		if err != nil {
			status := http.StatusInternalServerError
			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse(ja))
	}
}

// PostJobAdvert returns a gin handler func to create a new job advert.
func (h *Handler) PostJobAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		var ja types.JobAdvert

		if err := c.ShouldBindJSON(&ja); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := ja.Advert.Validate(); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := ja.Advert.NewID(); err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		if err := h.dao.CreateJobAdvert(&ja); err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusCreated, ja)
	}
}

// PutJobAdvert returns a gin handler func to update an existing job advert by ID.
func (h *Handler) PutJobAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		var ja types.JobAdvert

		if err := c.ShouldBindJSON(&ja); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := ja.Advert.Validate(); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		ja.Advert.ID = c.Param("id")
		ja.AdvertID = c.Param("id")

		if err := h.dao.UpdateJobAdvert(ja); err != nil {
			status := http.StatusInternalServerError

			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		uja, err := h.dao.GetJobAdvertByID(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, uja)
	}
}

// DeleteJobAdvert returns a gin handler func to delete a job advert by ID.
func (h *Handler) DeleteJobAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := h.dao.DeleteJobAdvertByID(c.Param("id")); err != nil {
			status := http.StatusInternalServerError

			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse("deleted"))
	}
}

// GetCarAdverts returns a gin hander func to list all car adverts.
func (h *Handler) GetCarAdverts() gin.HandlerFunc {
	return func(c *gin.Context) {
		ca, err := h.dao.GetCarAdverts()
		if err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse(ca))
		return
	}
}

// GetCarAdvert returns a gin handler func to get a car advert by its ID.
func (h *Handler) GetCarAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		ca, err := h.dao.GetCarAdvertByID(c.Param("id"))
		if err != nil {
			status := http.StatusInternalServerError
			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse(ca))
	}
}

// PostCarAdvert returns a gin handler func to create a new car advert.
func (h *Handler) PostCarAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		var ca types.CarAdvert

		if err := c.ShouldBindJSON(&ca); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := ca.Advert.Validate(); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := ca.Advert.NewID(); err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		cm, err := car.MatchWithModel(ca.CarModel)
		if err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		ca.Car = cm
		ca.CarBrand = cm.Brand.Brand
		ca.CarModel = cm.Model

		if err := h.dao.CreateCarAdvert(&ca); err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusCreated, ca)
	}
}

// PutCarAdvert returns a gin handler func to update an existing car advert by its ID.
func (h *Handler) PutCarAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		var ca types.CarAdvert

		if err := c.ShouldBindJSON(&ca); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := ca.Advert.Validate(); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		ca.Advert.ID = c.Param("id")
		ca.AdvertID = c.Param("id")

		cm, err := car.MatchWithModel(ca.CarModel)
		if err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		ca.Car = cm
		ca.CarBrand = cm.Brand.Brand
		ca.CarModel = cm.Model

		if err := h.dao.UpdateCarAdvert(ca); err != nil {
			status := http.StatusInternalServerError

			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		uja, err := h.dao.GetCarAdvertByID(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, uja)
	}
}

// DeleteCarAdvert returns a gin handler func to delete a car advert by ID.
func (h *Handler) DeleteCarAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := h.dao.DeleteCarAdvertByID(c.Param("id")); err != nil {
			status := http.StatusInternalServerError

			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse("deleted"))
	}
}

// GetPropertyAdverts returns a gin handler func to get all property adverts.
func (h *Handler) GetPropertyAdverts() gin.HandlerFunc {
	return func(c *gin.Context) {
		pa, err := h.dao.GetPropertyAdverts()
		if err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse(pa))
		return
	}
}

// GetPropertyAdvert returns a gin handler func to get a property advert by ID.
func (h *Handler) GetPropertyAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		pa, err := h.dao.GetPropertyAdvertByID(c.Param("id"))
		if err != nil {
			status := http.StatusInternalServerError
			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse(pa))
	}
}

// PostPropertyAdvert returns a gin handler func to create a property advert.
func (h *Handler) PostPropertyAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		var pa types.PropertyAdvert

		if err := c.ShouldBindJSON(&pa); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := pa.Advert.Validate(); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := pa.Advert.NewID(); err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		if err := h.dao.CreatePropertyAdvert(&pa); err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusCreated, pa)
	}
}

// PutPropertyAdvert returns a gin handler func to update an existing property
// advert by its ID.
func (h *Handler) PutPropertyAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		var pa types.PropertyAdvert

		if err := c.ShouldBindJSON(&pa); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		if err := pa.Advert.Validate(); err != nil {
			c.JSON(http.StatusBadRequest, newErrorResponse(http.StatusBadRequest, err))
			c.Error(err)
			return
		}

		pa.Advert.ID = c.Param("id")
		pa.AdvertID = c.Param("id")

		if err := h.dao.UpdatePropertyAdvert(pa); err != nil {
			status := http.StatusInternalServerError

			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		upa, err := h.dao.GetPropertyAdvertByID(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusInternalServerError, newErrorResponse(http.StatusInternalServerError, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, upa)
	}
}

// DeletePropertyAdvert returns a gin handler func to delete a property by ID.
func (h *Handler) DeletePropertyAdvert() gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := h.dao.DeletePropertyAdvertByID(c.Param("id")); err != nil {
			status := http.StatusInternalServerError

			if dao.IsNotFound(err) {
				status = http.StatusNotFound
			}

			c.JSON(status, newErrorResponse(status, err))
			c.Error(err)
			return
		}

		c.JSON(http.StatusOK, newDataResponse("deleted"))
	}
}
