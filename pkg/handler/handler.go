package handler

import "gitlab.com/Tomy2e/test-lbc/pkg/dao"

// Handler contains all gin handler funcs of the LBC app.
type Handler struct {
	dao *dao.DAO
}

// New returns a new Handler instance.
func New(dao *dao.DAO) *Handler {
	return &Handler{
		dao: dao,
	}
}
