package handler

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

// NoRoute returns a handler that sends a JSON 404 error.
func NoRoute() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(
			http.StatusNotFound,
			newErrorResponse(
				http.StatusNotFound,
				errors.New("page not found"),
			),
		)
	}
}
