package handler

// dataResponse contains the data response when no error occurs.
type dataResponse struct {
	Data any `json:"data"`
}

// newDataResponse returns a new data response with the specified data.
func newDataResponse(data any) dataResponse {
	return dataResponse{
		Data: data,
	}
}

// errorResponse contains the data response when an error occurs.
type errorResponse struct {
	Error errorData `json:"error"`
}

// errorData is the data that describes the error.
type errorData struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

// newErrorResponse returns a error response.
func newErrorResponse(status int, err error) errorResponse {
	return errorResponse{
		Error: errorData{
			Status:  status,
			Message: err.Error(),
		},
	}
}
