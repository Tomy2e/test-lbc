package dao

import (
	"gitlab.com/Tomy2e/test-lbc/pkg/types"
)

// GetPropertyAdvertByID finds a property advert by ID.
func (dao *DAO) GetPropertyAdvertByID(id string) (types.PropertyAdvert, error) {
	return getAdvertByID[types.PropertyAdvert](dao.db, id)
}

// GetPropertyAdverts lists all property adverts.
func (dao *DAO) GetPropertyAdverts() ([]types.PropertyAdvert, error) {
	return getAllAdverts[types.PropertyAdvert](dao.db)
}

// CreatePropertyAdvert creates a property advert.
func (dao *DAO) CreatePropertyAdvert(property *types.PropertyAdvert) error {
	return create(dao.db, property)
}

// UpdatePropertyAdvert updates an *existing* property advert.
func (dao *DAO) UpdatePropertyAdvert(property types.PropertyAdvert) error {
	return updateAdvert(dao.db, property.Advert, property)
}

// DeletePropertyAdvertByID deletes a property advert.
func (dao *DAO) DeletePropertyAdvertByID(id string) error {
	return deleteAdvertByID[types.PropertyAdvert](dao.db, id)
}

// GetJobAdvertByID finds a job advert by ID.
func (dao *DAO) GetJobAdvertByID(id string) (types.JobAdvert, error) {
	return getAdvertByID[types.JobAdvert](dao.db, id)
}

// GetJobAdverts lists all job adverts.
func (dao *DAO) GetJobAdverts() ([]types.JobAdvert, error) {
	return getAllAdverts[types.JobAdvert](dao.db)
}

// CreateJobAdvert creates a job advert.
func (dao *DAO) CreateJobAdvert(job *types.JobAdvert) error {
	return create(dao.db, job)
}

// UpdateJobAdvert updates an *existing* job advert.
func (dao *DAO) UpdateJobAdvert(job types.JobAdvert) error {
	return updateAdvert(dao.db, job.Advert, job)
}

// DeleteJobAdvertByID deletes a job advert.
func (dao *DAO) DeleteJobAdvertByID(id string) error {
	return deleteAdvertByID[types.JobAdvert](dao.db, id)
}

// GetCarAdvertByID finds a car advert by ID.
func (dao *DAO) GetCarAdvertByID(id string) (types.CarAdvert, error) {
	return getAdvertByID[types.CarAdvert](dao.db, id, "Car")
}

// GetCarAdverts lists all car adverts.
func (dao *DAO) GetCarAdverts() ([]types.CarAdvert, error) {
	return getAllAdverts[types.CarAdvert](dao.db, "Car")
}

// CreateCarAdvert creates a car advert.
func (dao *DAO) CreateCarAdvert(car *types.CarAdvert) error {
	return create(dao.db, car)
}

// UpdateCarAdvert updates an *existing* car advert.
func (dao *DAO) UpdateCarAdvert(car types.CarAdvert) error {
	return updateAdvert(dao.db, car.Advert, car)
}

// DeleteCarAdvertByID deletes a car advert.
func (dao *DAO) DeleteCarAdvertByID(id string) error {
	return deleteAdvertByID[types.CarAdvert](dao.db, id)
}
