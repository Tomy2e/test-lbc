package dao

import (
	"gitlab.com/Tomy2e/test-lbc/pkg/types"
	"gorm.io/gorm"
)

// getAdvertByID gets a generic advert by its ID. It is possible to preload
// fields by specifying their name. By default, the Advert field is
// automatically preloaded.
func getAdvertByID[T any](db *gorm.DB, id string, preloads ...string) (T, error) {
	var t T
	tx := db.Preload("Advert")

	for _, pre := range preloads {
		tx = tx.Preload(pre)
	}

	var err = tx.First(&t, "advert_id = ?", id).Error

	return t, err
}

// getAllAdverts lists all adverts of the specified type. It is possible to preload
// fields by specifying their name. By default, the Advert field is
// automatically preloaded.
func getAllAdverts[T any](db *gorm.DB, preloads ...string) ([]T, error) {
	var t []T
	tx := db.Preload("Advert")

	for _, pre := range preloads {
		tx = tx.Preload(pre)
	}

	var err = tx.Find(&t).Error

	return t, err
}

// save saves an item in the database.
func save(db *gorm.DB, item any) error {
	return db.Save(item).Error
}

// create creates an item.
func create(db *gorm.DB, item any) error {
	return db.Create(item).Error
}

// updateAdvert updates an existing generic advert. An error is returned
// if the advert doesn't exist.
func updateAdvert[T any](db *gorm.DB, advert types.Advert, item T) error {
	var t T

	return db.Transaction(func(tx *gorm.DB) error {
		if err := tx.First(&t, "advert_id = ?", advert.ID).Preload("Advert").Error; err != nil {
			return err
		}

		if err := tx.Model(&t).Select("*").Updates(&item).Error; err != nil {
			return err
		}

		return tx.Model(&advert).Select("*").Updates(&advert).Error
	})
}

// deleteAdvertByID deletes an existing generic advert.
func deleteAdvertByID[T any](db *gorm.DB, id string) (err error) {
	var t T

	err = db.Transaction(func(tx *gorm.DB) error {
		// Verify that the advert exists.
		if err := tx.First(&t, "advert_id = ?", id).Error; err != nil {
			return err
		}

		if err := tx.Delete(&t, "advert_id = ?", id).Error; err != nil {
			return err
		}

		return tx.Delete(&types.Advert{}, "id = ?", id).Error
	})

	return
}
