package dao

import (
	"database/sql/driver"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/Tomy2e/test-lbc/pkg/types"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func mockGorm() (*gorm.DB, sqlmock.Sqlmock, error) {
	db, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	gdb, err := gorm.Open(mysql.New(mysql.Config{
		SkipInitializeWithVersion: true,
		Conn:                      db,
	}), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	return gdb, mock, nil
}

type AnyTime struct{}

// Match satisfies sqlmock.Argument interface.
func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

func TestDAO_CreatePropertyAdvert(t *testing.T) {
	t.Parallel()

	db, mock, err := mockGorm()
	if err != nil {
		t.Fatalf("Failed to create gorm mock: %v", err)
	}

	dao := New(db)

	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO `adverts`").
		WithArgs("test", "test", "test", AnyTime{}, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectExec("INSERT INTO `property_adverts`").
		WithArgs("test").
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectCommit()

	if err := dao.CreatePropertyAdvert(&types.PropertyAdvert{
		Advert: types.Advert{
			ID:      "test",
			Title:   "test",
			Content: "test",
		},
	}); err != nil {
		t.Fatalf("Did not expect error: %v", err)
	}

	// we make sure that all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
