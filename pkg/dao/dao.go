package dao

import "gorm.io/gorm"

// DAO is a database access object. It contains methods to query the database.
type DAO struct {
	db *gorm.DB
}

// New returns a new instance of a DAO.
func New(db *gorm.DB) *DAO {
	return &DAO{
		db: db,
	}
}
