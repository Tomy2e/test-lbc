package dao

import (
	"errors"

	"gorm.io/gorm"
)

// IsNotFound returns true if err is not nil and it's caused by a record that
// is not found in the database.
func IsNotFound(err error) bool {
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return true
	}

	return false
}
