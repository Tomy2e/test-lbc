package dao

import (
	"errors"
	"testing"

	"gorm.io/gorm"
)

func TestIsNotFound(t *testing.T) {
	t.Parallel()
	type args struct {
		err error
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "nil error",
			args: args{},
			want: false,
		},
		{
			name: "is not found",
			args: args{
				err: gorm.ErrRecordNotFound,
			},
			want: true,
		},
		{
			name: "other error",
			args: args{
				err: errors.New("other error"),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			if got := IsNotFound(tt.args.err); got != tt.want {
				t.Errorf("IsNotFound() = %v, want %v", got, tt.want)
			}
		})
	}
}
