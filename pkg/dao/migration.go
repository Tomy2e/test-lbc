package dao

import (
	"fmt"

	"gitlab.com/Tomy2e/test-lbc/pkg/types"
)

// DoMigration automatically migrates the database. Tables are automatically
// created and updated.
func (dao *DAO) DoMigration() error {
	if err := dao.db.AutoMigrate(
		&types.CarBrand{},
		&types.CarModel{},
		&types.Advert{},
		&types.CarAdvert{},
		&types.JobAdvert{},
		&types.PropertyAdvert{},
		&types.CarBrand{},
		&types.CarModel{},
	); err != nil {
		return fmt.Errorf("migration failure: %w", err)
	}

	return nil
}
