package dao

import "gitlab.com/Tomy2e/test-lbc/pkg/types"

// SaveCarBrands saves the specified car brands in the database.
func (dao *DAO) SaveCarBrands(carBrands []types.CarBrand) error {
	return save(dao.db, carBrands)
}
