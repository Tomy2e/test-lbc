package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/Tomy2e/test-lbc/pkg/car"
	"gitlab.com/Tomy2e/test-lbc/pkg/dao"
	"gitlab.com/Tomy2e/test-lbc/pkg/handler"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// MySQLDSNEnv is the name of the environment variable that holds the MySQL DSN.
const MySQLDSNEnv = "MYSQL_DSN"

func main() {
	// Getting the MySQL DSN (connection string).
	dsn, ok := os.LookupEnv(MySQLDSNEnv)
	if !ok {
		log.Fatalf("%s env variable is not set", MySQLDSNEnv)
	}

	// Opening the MySQL connection.
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to open database: %v", err)
	}

	// Initializing the DAO and Handler.
	d := dao.New(db)
	hdl := handler.New(d)

	// Run database migration.
	if err := d.DoMigration(); err != nil {
		log.Fatalf("Failed to do migration: %v", err)
	}

	// Populate database with car brands and models.
	if err := d.SaveCarBrands(car.Brands()); err != nil {
		log.Fatalf("Failed to save cars in database: %v", err)
	}

	// Create default gin instance.
	r := gin.Default()
	r.NoRoute(handler.NoRoute())

	// Properties routes.
	properties := r.Group("/api/properties")
	properties.GET("", hdl.GetPropertyAdverts())
	properties.GET("/:id", hdl.GetPropertyAdvert())
	properties.POST("", hdl.PostPropertyAdvert())
	properties.PUT("/:id", hdl.PutPropertyAdvert())
	properties.DELETE("/:id", hdl.DeletePropertyAdvert())

	// Jobs routes.
	jobs := r.Group("/api/jobs")
	jobs.GET("", hdl.GetJobAdverts())
	jobs.GET("/:id", hdl.GetJobAdvert())
	jobs.POST("", hdl.PostJobAdvert())
	jobs.PUT("/:id", hdl.PutJobAdvert())
	jobs.DELETE("/:id", hdl.DeleteJobAdvert())

	// Cars routes.
	cars := r.Group("/api/cars")
	cars.GET("", hdl.GetCarAdverts())
	cars.GET("/:id", hdl.GetCarAdvert())
	cars.POST("", hdl.PostCarAdvert())
	cars.PUT("/:id", hdl.PutCarAdvert())
	cars.DELETE("/:id", hdl.DeleteCarAdvert())

	log.Fatal(r.Run())
}
