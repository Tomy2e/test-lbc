# Test LBC

Test LBC is a REST API to Create/Read/Update/Delete adverts in the following
categories:

- Jobs
- Properties
- Cars

The REST API is developed in Go using the following dependencies:

```text
github.com/gin-gonic/gin v1.8.1
github.com/go-ozzo/ozzo-validation/v4 v4.3.0
github.com/matoous/go-nanoid/v2 v2.0.0
github.com/texttheater/golang-levenshtein/levenshtein v0.0.0-20200805054039-cae8b0eaed6c
gopkg.in/Regis24GmbH/go-diacritics.v2 v2.0.2
gorm.io/driver/mysql v1.3.6
gorm.io/gorm v1.23.8
```

MariaDB/MySQL is used as the database.

## English to French glossary

- Advert: annonce
- Brand: marque
- Car: automobile
- Job: emploi
- Model: modèle
- Property: immobilier

## Database model

The logical database model is described in the following figure:

![Merise MLD](docs/images/merise_mld.png)

## Routes

### /api/properties

| Method | Request URL | Description                  |
| ------ | ----------- | ---------------------------- |
| GET    | /           | List all properties adverts  |
| GET    | /:id        | Get property advert by ID    |
| POST   | /           | Create a new property advert |
| PUT    | /:id        | Update property advert by ID |
| DELETE | /:id        | Delete property advert by ID |

### /api/jobs

| Method | Request URL | Description              |
| ------ | ----------- | ------------------------ |
| GET    | /           | List all jobs adverts    |
| GET    | /:id        | Get job advert by ID     |
| POST   | /           | Create a new jobs advert |
| PUT    | /:id        | Update job advert by ID  |
| DELETE | /:id        | Delete job advert by ID  |

### /api/cars

| Method | Request URL | Description             |
| ------ | ----------- | ----------------------- |
| GET    | /           | List all car adverts    |
| GET    | /:id        | Get car advert by ID    |
| POST   | /           | Create a new car advert |
| PUT    | /:id        | Update car advert by ID |
| DELETE | /:id        | Delete car advert by ID |

## Testing the app with Docker

### Pre-requisites

You need to build the Docker image:

```console
docker build -t test-lbc .
```

### Deploying the app

Run the following commands to deploy `test-lbc` and its MariaDB database.

```console
docker network create lbc-net

docker run --net lbc-net --name mariadb \
    -e MYSQL_ROOT_PASSWORD=root \
    -e MYSQL_DATABASE=lbc -d mariadb:10.5

docker run --net lbc-net --name test-lbc \
    -e MYSQL_DSN="root:root@tcp(mariadb:3306)/lbc?charset=utf8mb4&parseTime=True&loc=Local" \
    -d -p 8080:8080 test-lbc
```

Note: the database tables are automatically created by the `test-lbc` app, using
its automigration feature. It is not needed to import an sql file.

Make sure everyting is running:

```console
$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS          PORTS                                       NAMES
3a85aac0f017   test-lbc           "test-lbc"               21 seconds ago   Up 21 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   test-lbc
1bc71d52c4b5   mariadb:10.5       "docker-entrypoint.s…"   42 seconds ago   Up 42 seconds   3306/tcp                                    mariadb
```

The `test-lbc` app is now listening on the `8080` port of your machine.

### Sending requests

#### Properties

Create a property advert:

```console
$ curl -X POST localhost:8080/api/properties \
    -H 'Content-Type: application/json' \
    -d '{"advert":{"title":"My property", "content":"Description of my property"}}'
{"advert":{"id":"FB35xeKrlY8dMZA-yVUBA","title":"My property","content":"Description of my property","createdAt":"2022-09-04T13:27:46.706Z","updatedAt":"2022-09-04T13:27:46.706Z"}}
```

List all property adverts:

```console
$ curl localhost:8080/api/properties
{"data":[{"advert":{"id":"FB35xeKrlY8dMZA-yVUBA","title":"My property","content":"Description of my property","createdAt":"2022-09-04T13:27:46.706Z","updatedAt":"2022-09-04T13:27:46.706Z"}}]}
```

Update property advert:

```console
$ curl -X PUT localhost:8080/api/properties/FB35xeKrlY8dMZA-yVUBA \
    -H 'Content-Type: application/json' \
    -d '{"advert":{"title":"My property (updated)","content":"My new content"}}'
{"advert":{"id":"FB35xeKrlY8dMZA-yVUBA","title":"My property (updated)","content":"My new content","createdAt":"0001-01-01T00:00:00Z","updatedAt":"2022-09-04T13:32:18.196Z"}}
```

Get property advert by ID:

```console
$ curl localhost:8080/api/properties/FB35xeKrlY8dMZA-yVUBA
{"data":{"advert":{"id":"FB35xeKrlY8dMZA-yVUBA","title":"My property","content":"Description of my property","createdAt":"2022-09-04T13:27:46.706Z","updatedAt":"2022-09-04T13:27:46.706Z"}}}
```

Delete property advert by ID:

```console
$ curl -X DELETE localhost:8080/api/properties/FB35xeKrlY8dMZA-yVUBA
{"data":"deleted"}
```

#### Jobs

Create a job advert:

```console
$ curl -X POST localhost:8080/api/jobs \
    -H 'Content-Type: application/json' \
    -d '{"advert":{"title":"My job offer", "content":"Description of my job offer"}}'
{"advert":{"id":"zpezQjSUST99B2vVkpHx8","title":"My job offer","content":"Description of my job offer","createdAt":"2022-09-04T13:34:53.509Z","updatedAt":"2022-09-04T13:34:53.509Z"}}
```

List all job adverts:

```console
$ curl localhost:8080/api/jobs
{"data":[{"advert":{"id":"zpezQjSUST99B2vVkpHx8","title":"My job offer","content":"Description of my job offer","createdAt":"2022-09-04T13:34:53.509Z","updatedAt":"2022-09-04T13:34:53.509Z"}}]}
```

Update job advert:

```console
$ curl -X PUT localhost:8080/api/jobs/zpezQjSUST99B2vVkpHx8 \
    -H 'Content-Type: application/json' \
    -d '{"advert":{"title":"My job offer (updated)","content":"My new content"}}'
{"advert":{"id":"zpezQjSUST99B2vVkpHx8","title":"My job offer (updated)","content":"My new content","createdAt":"0001-01-01T00:00:00Z","updatedAt":"2022-09-04T13:35:30.924Z"}}
```

Get job advert by ID:

```console
$ curl localhost:8080/api/jobs/zpezQjSUST99B2vVkpHx8
{"data":{"advert":{"id":"zpezQjSUST99B2vVkpHx8","title":"My job offer (updated)","content":"My new content","createdAt":"0001-01-01T00:00:00Z","updatedAt":"2022-09-04T13:35:30.924Z"}}}
```

Delete job advert by ID:

```console
$ curl -X DELETE localhost:8080/api/jobs/zpezQjSUST99B2vVkpHx8
{"data":"deleted"}
```

#### Cars

Create a car advert:

```console
$ curl -X POST localhost:8080/api/cars \
    -H 'Content-Type: application/json' \
    -d '{"advert":{"title":"Citroen ds 3 crossback", "content":"Selling my citroen ds 3 crossback"}, "carModel":"ds 3 crossback"}'
{"advert":{"id":"xZUfUdSM8vOqEIWheJPXW","title":"Citroen ds 3 crossback","content":"Selling my citroen ds 3 crossback","createdAt":"2022-09-04T13:40:47.394Z","updatedAt":"2022-09-04T13:40:47.394Z"},"carModel":"Ds3","carBrand":"Citroen"}
```

List all car adverts:

```console
$ curl localhost:8080/api/cars
{"data":[{"advert":{"id":"xZUfUdSM8vOqEIWheJPXW","title":"Citroen ds 3 crossback","content":"Selling my citroen ds 3 crossback","createdAt":"2022-09-04T13:40:47.394Z","updatedAt":"2022-09-04T13:40:47.394Z"},"carModel":"Ds3","carBrand":"Citroen"}]}
```

Update car advert:

```console
$ curl -X PUT localhost:8080/api/cars/xZUfUdSM8vOqEIWheJPXW \
    -H 'Content-Type: application/json' \
    -d '{"advert":{"title":"Citroen ds3", "content":"Selling my car"}, "carModel":"ds 3 crossback"}'
{"advert":{"id":"xZUfUdSM8vOqEIWheJPXW","title":"Citroen ds3","content":"Selling my car","createdAt":"0001-01-01T00:00:00Z","updatedAt":"2022-09-04T13:42:25.846Z"},"carModel":"Ds3","carBrand":"Citroen"}
```

Get car advert by ID:

```console
$ curl localhost:8080/api/cars/xZUfUdSM8vOqEIWheJPXW
{"data":{"advert":{"id":"xZUfUdSM8vOqEIWheJPXW","title":"Citroen ds3","content":"Selling my car","createdAt":"0001-01-01T00:00:00Z","updatedAt":"2022-09-04T13:42:25.846Z"},"carModel":"Ds3","carBrand":"Citroen"}}
```

Delete car advert by ID:

```console
$ curl -X DELETE localhost:8080/api/cars/xZUfUdSM8vOqEIWheJPXW
{"data":"deleted"}
```
