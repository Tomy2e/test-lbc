# Build stage
FROM golang:1.19-alpine AS build
WORKDIR /app
COPY . .
RUN go build -o /test-lbc
# Final image
FROM alpine:3.14
WORKDIR /
COPY --from=build /test-lbc /usr/local/bin/test-lbc
USER 10001:10001
ENTRYPOINT ["test-lbc"]